package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.BindList;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (idPizza VARCHAR(128), idIng VARCHAR(128), "
			+ "FOREIGN KEY(idPizza) REFRENCES Pizzas(id),FOREIGN KEY(idIng) REFRENCES ingredients(id),"
			+ "PRIMARY KEY(idPizza,idIng));")
	void createAssociationTable();

	@Transaction
	default void createTableAndIngredientAssociation() {
		createPizzaTable();
		createAssociationTable();
	}

    @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
    void dropTable();

    @SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
    void insertPizza(Pizza pizza);
   
    @SqlUpdate("INSERT INTO PizzaIngredientsAssociation (idPizza, idIng) VALUES (:pizza.id, <ing.id>)")
    void insertPizzaIngredient(@BindBean("pizza") Pizza pizza, @BindList("ing") List<Ingredient> ingredient);
    
    @Transaction
	default void insert(Pizza pizza) {
    	insertPizza(pizza);
//    	for(Ingredient ingredient : pizza.getIngredients()) {
//        	insertPizzaIngredient(pizza, ingredient);
//		}
    	insertPizzaIngredient(pizza, pizza.getIngredients());
    }

    @SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
    void remove(@Bind("id") UUID id);

    @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);

    @SqlQuery("SELECT * FROM Pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);
    
    
}
