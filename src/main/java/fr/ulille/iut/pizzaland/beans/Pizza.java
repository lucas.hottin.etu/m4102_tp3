package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {

	private UUID id = UUID.randomUUID();
	private String name;
	private List<Ingredient> ingredients;

	public Pizza() {
	}

	public Pizza(String name) {
		this(name, null);
	}

	public Pizza(String name, UUID id) {
		this(name, id, null);
	}

	public Pizza(String name, UUID id, List<Ingredient> ingredients) {
		this.name = name;
		this.id = id;
		this.ingredients = ingredients;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void forEach(Consumer<? super Ingredient> action) {
		ingredients.forEach(action);
	}

	public int size() {
		return ingredients.size();
	}

	public boolean isEmpty() {
		return ingredients.isEmpty();
	}

	public boolean add(Ingredient e) {
		return ingredients.add(e);
	}

	public boolean remove(Object o) {
		return ingredients.remove(o);
	}

	public void clear() {
		ingredients.clear();
	}

	public Ingredient get(int index) {
		return ingredients.get(index);
	}

	public Ingredient remove(int index) {
		return ingredients.remove(index);
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setId(p.getId());
		dto.setName(p.getName());
		dto.setIngredients(p.getIngredients());
		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());
		pizza.setIngredients(dto.getIngredients());

		return pizza;
	}

	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizza.getName());

		return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());

		return pizza;
	}

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + ", ingredients=" + ingredients + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
