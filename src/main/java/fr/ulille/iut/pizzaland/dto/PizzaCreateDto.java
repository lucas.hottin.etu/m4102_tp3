package fr.ulille.iut.pizzaland.dto;

import java.util.List;

public class PizzaCreateDto {
	private String name;
	private List<IngredientDto> ingredients;
	public PizzaCreateDto() {}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}

	public List<IngredientDto> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<IngredientDto> ingredients) {
		this.ingredients = ingredients;
	}


}
